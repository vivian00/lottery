var path = require('path');
var webpack = require('webpack');
var config = require('./webpack.config.js');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const ROOT_PATH = path.resolve(__dirname);
const src = path.resolve(ROOT_PATH, 'src');
const dist = path.resolve(ROOT_PATH, 'dist');
const build = path.resolve(ROOT_PATH, 'build');

module.exports = {
  devtool: false,
  entry: {
    //配置入口文件，有几个写几个。我这里有两个文件。一个是所有我需要引入的文件，一个是我的入口文件，index.js
    //支持数组形式，将加载数组中的所有模块，但以最后一个模块作为输出,比如下面数组里面的js,全部压缩在了vendor这个文件这里
    vendors: ['jquery'],
    app: './src/index.js',
  },
  output: {
    path: path.join(__dirname, 'build'),
    // publicPath: '/',
    publicPath: '/static/', //模板、样式、脚本、图片等资源对应的server上的路径
    filename: 'bundle.[hash:8].js'
    // chunkFilename: 'js/[id].bundle.js'   //dundle生成的配置
  },
    loaders: [{
        test: /\.js?$/,
        exclude: /node_modules/,
        include: [src, './mock'],
        loader: 'babel'
      },
      {
        test: /\.scss$/,
           // 将 css 单独打包出来
        loader: ExtractTextPlugin.extract('css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass')
      }, {
        test: /\.css$/,
        // 将 css 单独打包出来
        loader: ExtractTextPlugin.extract('css!postcss'),
      }, {
        test: /\.(woff|woff2|ttf|eot|svg)\??.*$/,
        loader: 'url-loader?limit=8192&name=./fonts/[name].[ext]'
      }, {
        test: /\.(png|jpe?g|gif)$/,
        loader: 'url-loader?limit=8192&name=./imgs/[name].[ext]'
      }
    ]
  },
  // 编译 css 的时候加上 autoprefixer
  postcss: config.postcss,
  resolve: config.resolve,
  plugins: [
    // 全局的环境变量
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new ExtractTextPlugin('./css/style.[hash:8].css'),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "React": "react"
    }),
    // 加入 minify 压缩选项
    new HtmlWebpackPlugin({
      filename: './index.html',
      template: './src/index.html',
      title: 'Job',
      inject: true,
      hash: true,
      // chunks: ['vendor', 'app'], //需要引入的chunk，不配置就会引入所有页面的资源.名字来源于你的入口文件.
      minify: {
        removeComments: true,  // 移除HTML中的注释
        collapseWhitespace: true  // 删除空白符与换行符
      }
    }),
    // 压缩 JS，删除 console.log 之类
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        drop_console: true,
        drop_debugger: true
      }
    }),
    // 对比id的使用频率和分布来得出最短的id分配给使用频率高的模块
    new webpack.optimize.OccurenceOrderPlugin(),
    // 找到重复文件并去重。保证了重复的代码不被大包到bundle文件里面去，取而代之的是运行时请求一个封装的函数。
    new webpack.optimize.DedupePlugin(),
    //将引入的第三方库打包.这个要放到最后
    // vendors 对应 entry 部分，打包相应的文件
    // vendors.[hash:8].js 为打包生成的文件名
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.[hash:8].js'),
  ]
};
