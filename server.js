// webpack-dev-serve 是一个小型的用 Express 搭建成的 serve.
// 用 webpack-dev-middleware 来 serve webpack bundle 的。
// （ webpack-dev-middleware 是一个简单的包装webpack的中间件，
// 可以 watch 我们需要打包的文件，每次更新，打包成新的 bundle 都会通过它发出给serve。
// 它将文件都放在内存里，而不是磁盘中。
 // in serve.js
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config.js');
const PORT = 7070;
const API_TARGET = 'http://gpu.f3322.net:8000';
const server = new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  contentBase: 'dist',
  inline: true,
  port: PORT,
  noInfo: false,
  stats: { colors: true },
  historyApiFallback: true,
  proxy: {
    '/api/*': {
      target: API_TARGET,
      secure: false,
    },
  },
});

server.listen(PORT, '0.0.0.0', function (err) {
  if (err) {
    console.log(err);
  }
  console.info("==> 🌎  Listening on PORT %s. Open up http://localhost:%s/ in your browser.", PORT, PORT);
});
module.exports = server;
