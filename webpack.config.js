/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ROOT_PATH = path.resolve(__dirname);
const src = path.resolve(ROOT_PATH, 'src');
const dist = path.resolve(ROOT_PATH, 'dist');
const PORT = 7070;
module.exports = {
  devtool: 'eval-source-map',
  // 这个是和 webpack-dev-derver 结合起来用的
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:' + PORT,
    'webpack/hot/only-dev-server',
    './src/index.js',
  ],
// 这个是结合 express 的用法
  output: {
    // 生成的文件放置的地方
    path: dist,
    publicPath: '/static/',
    // 模板、样式、脚本、图片等资源对应的server上的路径
    //  生成的主文件的名字
    filename: 'bundle.js',
    //  chunkFilename: 'js/[id].bundle.js'   // dundle生成的配置
  },
  module: {
  // 先进行下检查
    preLoaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: /src/,
        loader: 'eslint',
      },
    ],
    loaders: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      include: [src, './mock', './*.js'],
      loader: 'react-hot!babel',
    },
    {
      test: /\.scss$/,
         // 将 css 单独打包出来
      loader: ExtractTextPlugin.extract('css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap'),
    }, {
      test: /\.css$/,
      // 将 css 单独打包出来
      loader: ExtractTextPlugin.extract('style-loader', 'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!'),
    }, {
      test: /\.(woff|woff2|ttf|eot|svg)\??.*$/,
      loader: 'url-loader?limit=8192&name=./fonts/[name].[ext]',
    }, {
      test: /\.(png|jpe?g|gif)$/,
      loader: 'url-loader?limit=8192&name=./imgs/[name].[ext]',
    }],
  },
  // 编译 css 的时候加上 autoprefixer
  postcss: function () {
    return [require('autoprefixer')];
  },
  // 这里注意 第一个 参数是 '' ,里面没有空格
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEVTOOLS__: !!process.env.DEBUG
    }),
  // production 环境下加  hash
    new ExtractTextPlugin('./css/style.css'),
    // 热替换
    new webpack.HotModuleReplacementPlugin(),
    //  dev 时候有错误提示，但不会自动停掉 编译 /server 服务
    new webpack.NoErrorsPlugin(),
    // 全局变量，这样在用 React 的时候就不用每一个文件前面都加 import 了。
    new webpack.ProvidePlugin({
      React: 'react',
    }),
    new HtmlWebpackPlugin({
      filename: './index.html',
      template: './src/index.html',
      title: 'lottery',
      inject: true,
      hash: true,
    }),
  ],
};
