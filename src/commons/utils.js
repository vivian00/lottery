/* eslint-disabled func-names */
/**
 * [1,2,5] => [0, 1, 1, 0, 0, 1]
 *
 * @param {Array} [array] [需要 format 的原数组]
 * @param {Number} [len] [输出的数组长度]
 * @return {Array}
 *
*/
const formatNumsArray = function formatNumsArray(array, len) {
  const newArray = [];
  newArray.length = len;
  newArray.fill(0);

  array.forEach((num) => {
    newArray[num - 1] = num;
  });

  // console.log(newArray);
  return newArray;
};
/**
 *
 * 将输入的数组 [n1, n2, n3] 输出成 object {n1: [0], n2: [1], n3: [3] }
 * @param {Array} [array] [输入的 数组]
 * @return {Object} [array => object]
 *
 */

const arrayToObject = function arrayToObject(array) {
  return array.reduce((pre, cur, idx) => {
    let rst = {
      ...pre,
    };

    if (pre[cur] && pre[cur].length > 0) {
      rst[cur].push(idx);
    } else {
      rst = {
        ...rst,
        [cur]: [idx],
      };
    }
    return rst;
  }, {});
};

/**
 *
 * 判断输入的数组 [1, 0, 1, 2, 1, 1, 1, 1, 1]
 * 是否有 count 个连着的元素, 不等于 0.
 * @param {Array} [array] [监测的数组]
 * @param {Number} [count] [连着的个数]
 * @return 新的数组 [{num: 1, link: false},
 * {num: 0, link: false},
 * {num: 1, link: true},
 * {num: 2, link: true}
 * ... ]
 *
*/
const checkArrayLink = (array, count) => {
  const newArray = [];
  // console.log(`array: ${array}`);
  const len = array.length;

  for (let i = 0; i < len;) {
    if ((i < (array.length - count) + 1) && array[i] !== 0) {
      const tempArray = array.slice(i, i + count);
      // console.log(`tempArray: ${tempArray}`);

      const isLink = tempArray.every((item) => item !== 0);

      if (isLink) {
        let j = i;
        for (j; (array[j] !== 0) && j < len; j += 1) {
          // console.log(`j: ${j}; num: ${array[j]}`);
          newArray.push({
            num: array[j],
            link: true,
          });
        }
        i = j;
      } else {
        newArray.push({
          num: array[i],
          link: false,
        });
        i += 1;
      }
    } else {
      newArray.push({
        num: array[i],
        link: false,
      });
      i += 1;
    }
  }
  // console.log(newArray);
  return newArray;
};
/**
 * 二维矩阵竖向 (列) 统计，不为 0 的元素
 * @param {Array} [array] [待统计的数组]
 * @return {Array} [统计好的数组]
 * 如：
 * 输入
 * [
 *   [1, 0, 1, 3],
 *   [1, 1, 0, 1],
 *   [1, 0, 1, 1],
 * ]
 * 输出
 * [
 *   {
 *     num: 0,
 *     sum: 3,
 *   },
 *   {
 *     num: 1,
 *     sum: 1,
 *   },
 *   {
 *     num: 2,
 *     sum: 2,
 *   },
 *   {
 *     num: 3,
 *     sum: 3,
 *   },
 * ]
 *
 */

const verticalSum = (array = []) => {
  const sumList = [];
  // console.log(array);
  // console.log(array[0]);
  if (Array.isArray(array) && Array.isArray(array[0])) {
    const colLen = array.length;
    // console.log(`colLen: ${colLen}`); // y
    const rowLen = array[0].length; // x
    // console.log(`rowLen: ${rowLen}`);
    for (let i = 0; i < rowLen; i += 1) {
      // console.log(`i: ${i}`);
      let tempCount = 0;
      for (let j = 0; j < colLen; j += 1) {
        // console.log(`array: ${array[j]}`);
        // console.log(array[j][i]);
        tempCount += (array[j][i] !== 0) ? 1 : 0;
      }

      sumList.push({
        num: i,
        sum: tempCount,
      });
    }
    // console.log('verticalSum');
    // console.log(sumList);
  }
  return sumList;
};

/*
  max: {
  num: xx,
  indexs: [],
}
 */
const findMaxMinTwo = (formatArray = []) => {
  const rst = formatArray.reduce((pre, { num, sum }, idx) => {
    // console.log(pre);
    // if (!pre.max) {
    //
    //   return {
    //     ...pre,
    //
    //   }
    // }

    if (!pre.max) {
      return {
        max: {
          sum,
          idx,
        },
        min: {
          sum,
          idx,
        },
        secondMax: {
          sum,
          idx,
        },
        secondMin: {
          sum,
          idx,
        },
      };
    }

    // console.log(num);
    // console.log(sum);
    if (sum < pre.secondMin.sum) {
      // console.log('min');
      const tempMin = {
        sum,
        idx,
      };
      if (sum < pre.min.sum) {
        return {
          ...pre,
          secondMin: {
            ...pre.min,
          },
          min: tempMin,
        };
      }

      return {
        ...pre,
        secondMin: tempMin,
      };
    }

    // console.log('max');
    if (sum > pre.secondMax.sum) {
      const tempMax = {
        sum,
        idx,
      };
      if (sum > pre.max.sum) {
        return {
          ...pre,
          secondMax: {
            ...pre.max,
          },
          max: tempMax,
        };
      }

      return {
        ...pre,
        secondMax: tempMax,
      };
    }
    return pre;
  }, {});
  // console.log(rst);
  return rst;
};

/*
 *
 * 寻找两个 数组 中 相同元素
 * 返回 相同的元素 的数组
 */
const findRep = (array1, array2) => {
  array1.sort();
  array2.sort();
  const temp = [];
  array1.forEach((item) => {
    if (array2.indexOf(item)) {
      temp.push(item);
    }
  });
  return temp;
};

export default {
  formatNumsArray,
  arrayToObject,
  checkArrayLink,
  verticalSum,
  findMaxMinTwo,
  findRep,
};
