const BASE_API = '/api';
const TIME_LIMIT = 2000;
export default {
  BASE_API,
  TIME_LIMIT,
};
