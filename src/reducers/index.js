import { combineReducers } from 'redux';

import { BEFORE_REQUEST_POST, RECEIVE_POST, TEST_ACTION, CHANGE_MENU_SELECTED, TOGGLE_MENU } from '../actions/tableActions';
import { RECEIVE_SEARCH } from '../actions/searchAction';

const defaultMenuState = {
  index: 0,
  showMenu: false,
};

const menuRecord = (state = defaultMenuState, action = {}) => {
  switch (action.type) {
    case CHANGE_MENU_SELECTED:
      return {
        ...state,
        showMenu: false,
        index: action.index,
      };
    case TOGGLE_MENU:
      return {
        ...state,
        showMenu: action.data,
      };
    default:
      return state;
  }
};

const defaultState = {
  isLoading: true,
  text: '',
  stat: [
    {
      freq: 9,
      label: 1,
    },
  ],
  tableDatas: [
    {
      nums: [],
      seq: '',
      issue: '',
      time: '',
    },
  ],
};

const table = (state = defaultState, action) => {
  switch (action.type) {
    case TEST_ACTION:
      return {
        ...state,
        text: action.data,
      };
    case BEFORE_REQUEST_POST: {
      if (state.tableDatas.length > 1 ||
        (state.tableDatas.length === 1 &&
        state.tableDatas[0].nums.length > 0)) {
        return {
          ...state,
        };
      }
      return {
        ...state,
        isLoading: true,
      };
    }
    case RECEIVE_POST: {
      const { stat, data } = action.data;
      return {
        ...state,
        stat,
        tableDatas: [...data],
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

const defaultSearchState = {
  searchRes: [
    {
      searchNum: ['03', '05', '09', '10', '14', '12'],
      searchDatas: [
        {
          x: 0,
          y: 10,
        },
      ],
    },
  ],
};

const search = (state = defaultSearchState, action) => {
  switch (action.type) {
    case RECEIVE_SEARCH: {
      const { searchRes } = action.data;
      return {
        ...state,
        searchRes,
      };
    }
    default:
      return state;
  }
};

export default combineReducers({ table, menuRecord, search });
