import React from 'react';
import classnames from 'classnames';
import styles from './freqTd.scss';

// datas = [[array1], [array2]...[array20]]
// 每一个 array 是 20 个元素
const FreqTd = ({ datas }) => {
  // console.log('freqList');
  // console.log(props);
  const freqList = datas.map(({ freq, label }, idx) => {
    const style = classnames({
      [styles.topOne]: label === 1,
      [styles.topTwo]: label === 2,
      [styles.lastOne]: label === -1,
      [styles.lastTwo]: label === -2,
    });
    return (<li
      key={idx}
      className={style}
    >
      {freq}
    </li>);
  });

  return (
    <section className={styles.freq}>
      <div className={styles.title}>概率统计</div>
      <ul className={styles.freqList}>
        {freqList}
      </ul>
    </section>
  );
};

FreqTd.propTypes = {
  datas: React.PropTypes.array,
};

export default FreqTd;
