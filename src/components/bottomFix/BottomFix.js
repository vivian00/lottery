import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tableActions from '../../actions/tableActions';

import styles from './bottomFix.scss';

import FreqTd from './freqTd/FreqTd';
import Publish from './publish/Publish';
import CONST from '../../commons/const';

class BottomFix extends React.PureComponent {
  constructor(props) {
    super(props);
    this.displayName = 'BottomFix';
    this.url = `${CONST.BASE_API}/data/latest`;
  }

  componentWillMount() {
    this.props.fetchDatas(this.url);
  }

  render() {
    const wrapStyle = classnames(`${styles.wrap}`, `${styles.topShadow}`);
    let freq = null;
    if (this.props.path === '/' || this.props.path === 'main' || this.props.path === '/main') {
      freq = <FreqTd datas={this.props.stat} />;
    }

    return (
      <div className={wrapStyle}>
        {freq}
        <Publish
          latestNums={this.props.latestData}
          toggleMenu={this.props.toggleMenu}
          showMenu={this.props.showMenu}
        />
      </div>
    );
  }
}

BottomFix.propTypes = {
  latestData: React.PropTypes.object,
  stat: React.PropTypes.array,
  showMenu: React.PropTypes.bool,
  toggleMenu: React.PropTypes.func,
  fetchDatas: React.PropTypes.func,
  path: React.PropTypes.string,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(tableActions, dispatch);

export default connect(null, mapDispatchToProps)(BottomFix);

// export default BottomFix;
