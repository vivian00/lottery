import React from 'react';
import classnames from 'classnames';
import styles from './publish.scss';
import drawLottery from '../../../assests/images/draw_lottery.png';
// import drawLotterySVG from '../../../assests/svg/draw_lottery.svg';

/* eslint-disable jsx-a11y/no-static-element-interactions */
class Publish extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = Publish;
    this.onhandleClick = this.onhandleClick.bind(this);
  }

  onhandleClick(e) {
    e.preventDefault();
    const { toggleMenu, showMenu } = this.props;
    toggleMenu(!showMenu);
  }
  render() {
    console.log('publish', this.props);
    const assistStyle = classnames('iconfont', 'icon-icon13', `${styles.icon_assist}`);
    const { nums, seq, time } = this.props.latestNums;
    const reg = /(\d+):(\d+):/;
    const timeArray = time.match(reg);
    let hr;
    let min;
    if (time) {
      hr = timeArray[1];
      min = timeArray[2];
    }
    // <svg>
    //   <use xlink:href="../../../assests/svg/draw_lottery.svg#draw-lottery" />
    // </svg>

    const numsList = nums.map((num, idx) => <li key={idx}>{num}</li>);
    return (
      <section className={styles.wrap}>
        <div className={styles.timeWrap}>
          <span className={styles.title}>上次开奖</span>
          <p className={styles.time}>
            <span>{hr}</span>
            :
            <span>{min}</span>
          </p>
        </div>
        <div className={styles.nums}>
          <p className={styles.title}>第 {seq} 期
            <img className={styles.flag} src={drawLottery} alt="draw lottery" />
          </p>
          <ul className={styles.numsList}>
            {numsList}
          </ul>
        </div>
        <div
          className={styles.assist}
        >
          <button
            className={styles.btn}
            onClick={this.onhandleClick}
          >
            <span className={assistStyle} />
            <p className={styles.title}> 助手 </p>
          </button>
        </div>
      </section>
    );
  }
}

Publish.propTypes = {
  latestNums: React.PropTypes.object,
  toggleMenu: React.PropTypes.func,
  showMenu: React.PropTypes.bool,
};

export default Publish;
