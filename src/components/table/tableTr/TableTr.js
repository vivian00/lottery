import React from 'react';
import classnames from 'classnames';
import utils from '../../../commons/utils';
import styles from './tableTr.scss';


class TableTr extends React.PureComponent {
  static propTypes = {
    datas: React.PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.displayName = 'TableTr';
    this.props = props;

    // 元素 linkMin 个连续的时候, className = 'link'
    this.linkMin = 3;
  }

  render() {
    const { seq, three, nums, d, odd } = this.props.datas;
    const threeList = three.map((num, idx) => <td key={idx}>{num}</td>);

    const newNums = utils.formatNumsArray(nums, 20);
    // format 前三位
    const threeListObject = utils.arrayToObject(three);

    const linkArray = utils.checkArrayLink(newNums, this.linkMin);

    const numsList = linkArray.map(({ num, link }, idx) => {
      const style = classnames({
        [`${styles.import}`]: threeListObject[num] && threeListObject[num].length > 0,
        [`${styles.link}`]: link,
      });

      return (
        <td key={idx} className={style}>
          {num !== 0 ? num : ''}
        </td>);
    });
    // const numsList = newNums.map((num, idx) => <td key={idx}>{num === 0 ? '' : num}</td>);
    return (
      <tr className={styles.tr}>
        <td>{seq}</td>
        {threeList}
        {numsList}
        <td>{d}</td>
        <td>{odd}</td>
      </tr>
    );
  }
}

export default TableTr;
