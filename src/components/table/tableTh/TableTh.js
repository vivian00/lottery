import React from 'react';
import styles from './tableTh.scss';

const TableTh = () => {
  const list = [
    {
      colspan: 1,
      title: '期数',
    },
    {
      colspan: 3,
      title: '前三位',
    },
    {
      colspan: 10,
      title: '小区',
    },
    {
      colspan: 10,
      title: '大区',
    },
    {
      colspan: 1,
      title: '重号',
    },
    {
      colspan: 1,
      title: '奇数',
    },
  ];
  const thList = list.map(({ colspan, title }) =>
    (<th colSpan={colspan} className="th2" key={title}><span>{ title }</span></th>));
  return (
    <thead className={styles.thead}>
      <tr className={styles.th1}>
        {thList}
      </tr>
    </thead>
  );
};

export default TableTh;
