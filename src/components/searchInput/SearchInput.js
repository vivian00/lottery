import React from 'react';
import classnames from 'classnames';
import styles from './searchInput.scss';

class SearchInput extends React.PureComponent {
  constructor(props) {
    super(props);
    this.displayName = 'SearchInput';
  }

  render() {
    const iconStyle = (state) => classnames('iconfont', `${styles.iconfont}`,
      `${state === 'shang' ? 'icon-shang' : 'icon-xia'}`);

    return (
      <form
        method="post"
        className={styles.wrap}
      >
        <input
          type="text"
          placeholder="数字间使用空格隔开, 例如: 16 17 18"
          className={styles.input}
        />
        <div className={styles.multiSelect}>
          <i className={iconStyle('shang')} />
          <p> 任二 </p>
          <span className={iconStyle('xia')} />
        </div>
        <input
          type="button"
          className={styles.submit}
          value="搜索"
          onClick={this.props.submit}
        />
      </form>
    );
  }
}

SearchInput.propTypes = {
  submit: React.PropTypes.func,
};

export default SearchInput;
