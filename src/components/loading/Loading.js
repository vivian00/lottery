import React from 'react';
import styles from './loading.scss';
import Src1 from '../../assests/images/loading1.png';
import Src2 from '../../assests/images/loading2.png';

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Loading';
    this.state = {
      imgSrc: Src1,
    };
    this.fun = this.fun.bind(this);
    this.timer = setTimeout(this.fun, 600);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  fun() {
    clearTimeout(this.timer);
    if (!this.state.imgSrc) {
      this.setState({
        imgSrc: Src1,
      });
    } else {
      this.setState({
        imgSrc: this.state.imgSrc === Src2 ? Src1 : Src2,
      });
    }
    console.log(this.state.imgSrc);
    this.timer = setTimeout(this.fun, 600);
  }

  render() {
    console.log(this.state);
    return (
      <div className={styles.wrap}>
        <img alt="加载中" src={this.state.imgSrc} className={styles.img} />
      </div>
    );
  }
}

export default Loading;
