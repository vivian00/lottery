import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tableActions from '../../actions/tableActions';

import styles from './topFix.scss';

const recommend = [2, 4, 5, 9, 13, 17, 19, 20];
const q = 12;

class TopFix extends React.PureComponent {
  constructor(props) {
    super(props);
    this.displayName = 'TopFix';
  }

  render() {
    // const recommendList = recommend.map()
    const pageCount = 1;
    const pageSum = 4;

    const cName = classnames(`${styles.topFix}`, {
      [`${styles.shadow}`]: true,
    });
    const recommendList = recommend.map((item, idx) => <i key={idx}>{item}</i>);

    return (
      <div className={cName}>
        <span className={styles.text}>
          号码推荐: {recommendList}
        </span>
        <span className={styles.text}>
          第{q}期推荐已中奖
        </span>
        <span className={styles.pages}>
          {pageCount}/{pageSum}
        </span>
      </div>
    );
  }
}

TopFix.propTypes = {
  data: React.PropTypes.object,
  fetchDatas: React.PropTypes.func,
};

const mapStateToProps = state => ({
  data: state.table.tableDatas[0],
});

const mapDispatchToProps = (dispatch) => bindActionCreators(tableActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TopFix);
