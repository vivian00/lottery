import React from 'react';
import echarts from 'echarts';
import styles from './chart.scss';
/* eslint-disable */
class Chart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.displayName = 'Chart';
  }

  componentDidMount() {
    console.log('componentDidMount');
    // 初始化
    const myChart = echarts.init(this.chartBlock);
    const option = {
      title: { text: 'ECharts 入门示例' },
      tooltip: {},
      xAxis: {
          data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
      },
      yAxis: {},
      series: [{
          name: '销量',
          type: 'bar',
          data: [5, 20, 36, 10, 10, 20]
      }],
    };
    console.log(echarts);
    console.log(myChart);
    // 绘制图表
    myChart.setOption(option);
  }

  render() {
    console.error('render');

    return (
      <div className={styles.wrap} ref={(o) => { this.chartBlock = o; }}>
      </div>
    );
  }
}

// Main.propTypes = {
//   tableDatas: React.PropTypes.any,
//   testAction: React.PropTypes.func,
//   fetchDatas: React.PropTypes.func,
// };

export default Chart;
