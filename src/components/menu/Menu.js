import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import styles from './menu.scss';

import * as tableActions from '../../actions/tableActions';

const MenuItem = (props) => {
  const { name, icon, url, isSelected, id } = props;

  const iconClass = classnames('iconfont', `${icon}`, `${styles.iconfont}`);

  const liClass = classnames('clearfix',
  { [styles.active]: isSelected });

  const rightIconClass = classnames('iconfont', 'icon-youbian', `${styles.right_icon}`);

  const onhandleClick = () => {
    props.onHandleChangeMenu(id);
  };

  return (
    <li className={liClass} >
      <Link
        to={`${url}`}
        onClick={onhandleClick}
      >
        <i className={iconClass} />
        {name}
        <span className={rightIconClass} />
      </Link>
    </li>
  );
};

MenuItem.propTypes = {
  id: React.PropTypes.number,
  // id: React.PropTypes.oneOfType([React.PropTypes.ststring, React.PropTypes.number]),
  name: React.PropTypes.string,
  url: React.PropTypes.string,
  icon: React.PropTypes.string,
  isSelected: React.PropTypes.bool,
  onHandleChangeMenu: React.PropTypes.func,
};

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Menu';

    this.state = {
      selected: 0,
    };
    this.menuTitles = [
      {
        name: '号码走势图',
        icon: 'icon-zoushi1',
        url: 'main',
      },
      {
        name: '遗漏查询',
        icon: 'icon-chaxun',
        url: 'search',
      },
      {
        name: '复式查询',
        icon: 'icon-xunhuanjiantou',
        url: './muti-search',
      },
      {
        name: '当前遗漏统计',
        icon: 'icon-zhuzhuangtu-copy',
        url: './current-search',
      },
      {
        name: '冷热统计',
        icon: 'icon-weibiaoti2',
        url: './hot ',
      },
      {
        name: '追号计划',
        icon: 'icon-xin',
        url: './plans',
      },
    ];
  }

  render() {
    console.warn(this.props.index);
    const menuList = this.menuTitles.map((item, idx) => (
      <MenuItem
        key={idx} {...item}
        id={idx}
        isSelected={this.props.index === idx}
        onHandleChangeMenu={this.props.changeMenu}
      />));
    return (
      <div>
        <div className={styles.wrap}>
          <ul className={styles.list}>
            {menuList}
          </ul>
        </div>
      </div>
    );
  }
}

Menu.propTypes = {
  index: React.PropTypes.number,
  changeMenu: React.PropTypes.func,
};

const mapStateToProps = state => ({
  index: state.menuRecord.index,
});

// const mapDispatchToProps = (dispatch) => bindActionCreators({
const mapDispatchToProps = (dispatch) => bindActionCreators({
  changeMenu: tableActions.changeMenuSelected,
}, dispatch);
// const mapDispatchToProps = (dispatch) => bindActionCreators(tableActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
