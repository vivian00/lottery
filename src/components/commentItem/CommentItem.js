/* eslint-disable  */
import React from 'react';

export default class CommentItem extends React.Component {

  render() {
    const { id, avatar, name, praise, text, time, mpraise } = this.props.data;
    console.log('commentItem');

    return (
      <li>
        <img src={avatar} alt="avatar" />
        <div>
          <p>
            <span
              data-num={praise}
              data-me={mpraise}
              onClick={this.props.onAddPraise.bind(this, id)}
              >
              {mpraise ?`我和${praise-1}个人` : `${praise}个人`} 赞
            </span>
          </p>
          <p >{name}</p>
          <p>
            {text}
            <span> {time}</span>
          </p>
        </div>
      </li>
    );
  };
};

CommentItem.propTypes = {
  data: React.PropTypes.shape({
    id: React.PropTypes.number,
    name: React.PropTypes.string,
    text: React.PropTypes.string,
    time: React.PropTypes.string,
    praise: React.PropTypes.number,
    mpraise: React.PropTypes.oneOf([0,1]),
  }),
  onAddPraise: React.PropTypes.func,
};
