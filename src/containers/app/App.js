import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
/* eslint-disable */
import Swiper from 'react-swiper';
/* eslint-enable */
import { bindActionCreators } from 'redux';
import * as tableActions from '../../actions/tableActions';
import CONST from '../../commons/const';
import TopFix from '../../components/topFix/TopFix';
import BottomFix from '../../components/bottomFix/BottomFix';
import Menu from '../../components/menu/Menu';
import styles from './app.scss';
/* eslint-disable */

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.displayName = 'App';

    this.direction = null;
    this.initX = null;
    this.initY = null;

    this.endX = null;
    this.endY = null;

    this.handleTouchStart = this.handleTouchStart.bind(this);
    this.handleTouchMove = this.handleTouchMove.bind(this);
    this.handleTouchEnd = this.handleTouchEnd.bind(this);
    this.url = `${CONST.BASE_API}/data/latest`;
  }

  componentWillMount() {
    const { circleFetchDatas, tableDatas } = this.props;
    const params = {
      'synced-issue': tableDatas[0].issue || -1,
    };
    circleFetchDatas(this.url, params);
  }

  handleTouchMove(e) {
    console.warn('handleTouchMove');
    const touch = e.touches[0];
    this.direction = touch.pageX - this.initX;
    this.directionY = touch.pageY - this.initY;
    console.log('directionY', this.directionY);
    console.log(this.scrollWrap.scrollTop);
    console.log(this.scrollWrap.offsetHeight);

    // 在顶上
    if (this.scrollWrap.scrollTop < 0 && this.directionY > 0) {
      e.preventDefault();
    }
      // 在底部
    const scrollWrapHeight = this.scrollWrap.offsetHeight;
    if (this.scrollWrap.scrollTop > scrollWrapHeight && this.directionY < 0) {
      e.preventDefault();
    }

    if (!this.props.showMenu && this.direction < 0 && Math.abs(this.direction) > 30) {
      // console.warn('OPEN MENU');
      this.props.toggleMenu(true);
    }
    if (this.props.showMenu && this.direction > 0 && Math.abs(this.direction) > 30) {
      // console.warn('CLOSE MENU');
      this.props.toggleMenu(false);
    }
  }

  handleTouchStart(e) {
    const touch = e.touches[0];
    // this.touch = touch;
    this.initX = touch.pageX;
    this.initY = touch.pageY;
  }

  handleTouchEnd() {
    this.initData();
  }

  initData() {
    this.initX = null;
    this.initY = null;
    this.direction = null;
  }

  render() {
    const slideStyle = classnames(`${styles.slide}`, {
      [styles.show]: this.props.showMenu,
    });
    const len = this.props.tableDatas.length;
    // return (
    //   <div
    //     className={styles.wrap}
    //   >
    //     <div
    //       className={slideStyle}
    //     >
    //       <div className={styles.main_content}>
    //         {this.props.children}
    //       </div>
    //       <div className={styles.slide_menu}>
    //         <Menu />
    //       </div>
    //     </div>
    //   </div>
    // );
          // <div
          //   className={styles.wrap}
          //   onTouchStart={this.handleTouchStart}
          //   onTouchMove={this.handleTouchMove}
          //   onTouchEnd={this.handleTouchEnd}
          // >
    return (
      <div
        className={styles.wrap}
        onTouchStart={this.handleTouchStart}
        onTouchMove={this.handleTouchMove}
        onTouchEnd={this.handleTouchEnd}
        ref={o => {this.scrollWrap = o;}}
      >
        <div
          className={slideStyle}
        >
          <div className={styles.main_content}>
            <TopFix />
            <div className={styles.content_body}>
              {this.props.children}
            </div>
            <BottomFix
              latestData={this.props.tableDatas[len - 1]}
              stat={this.props.stat}
              showMenu={this.props.showMenu}
              toggleMenu={this.props.toggleMenu}
              path={this.props.location.pathname}
            />
          </div>
          <div className={styles.slide_menu}>
            <Menu />
          </div>
        </div>
      </div>
    );
  }
}
App.propTypes = {
  showMenu: React.PropTypes.bool,
  tableDatas: React.PropTypes.array,
  stat: React.PropTypes.array,
  toggleMenu: React.PropTypes.func,
  location: React.PropTypes.object,
  circleFetchDatas: React.PropTypes.func,
};

const mapStateToProps = state => ({
  showMenu: state.menuRecord.showMenu,
  tableDatas: state.table.tableDatas,
  stat: state.table.stat,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  toggleMenu: tableActions.toggleMenu,
  circleFetchDatas: tableActions.circleFetchDatas,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
