import React from 'react';
// import styles from './search.scss';
import SearchInput from '../../components/searchInput/SearchInput';
/* eslint-disable */

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Search';
    this.onhandleSumbit = this.onhandleSumbit.bind(this);
  }

  onhandleSumbit() {
    console.log('get Search!');
  }

  render() {
    return (
      <div>
        <SearchInput submit={this.onhandleSumbit} />
      </div>
    );
  }
}

// Main.propTypes = {
//   tableDatas: React.PropTypes.any,
//   testAction: React.PropTypes.func,
//   fetchDatas: React.PropTypes.func,
// };
//
// //  对从 redux 的 store 来的 state 做过滤
// const mapStateToProps = state => ({
//   tableDatas: state.table.tableDatas,
// });
//
// const mapDispatchToProps = (dispatch) => bindActionCreators(tableActions, dispatch);


export default Search;
