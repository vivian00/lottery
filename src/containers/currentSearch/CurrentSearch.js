import React from 'react';
// import styles from './search.scss';
import SearchInput from '../../components/searchInput/SearchInput';
import Chart from '../../components/chart/Chart';
/* eslint-disable */

class CurrentSearch extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'CurrentSearch';
    this.onhandleSumbit = this.onhandleSumbit.bind(this);
  }

  onhandleSumbit() {
    console.log('get CurrentSearch!');
  }

  render() {
    return (
      <div>
        <SearchInput submit={this.onhandleSumbit} />
        <div>
          charts list
          <Chart />
        </div>
      </div>
    );
  }
}

// Main.propTypes = {
//   tableDatas: React.PropTypes.any,
//   testAction: React.PropTypes.func,
//   fetchDatas: React.PropTypes.func,
// };
//
// //  对从 redux 的 store 来的 state 做过滤
// const mapStateToProps = state => ({
//   tableDatas: state.table.tableDatas,
// });
//
// const mapDispatchToProps = (dispatch) => bindActionCreators(tableActions, dispatch);


export default CurrentSearch;
