/* global __DEVTOOLS__ */
/*eslint-disable*/
import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import configureStore from '../store/configureStore';
import routes from '../routes';

const store = configureStore();

function createElements(history) {
  const elements = [
    <Router
      key="router"
      history={history}
      routes={routes} />
  ];

  if (typeof __DEVTOOLS__ !== 'undefined' && __DEVTOOLS__) {

    const DevTools = require('./DevTools').default;
    // elements.push(<DevTools key="devtools" />);
  }

  return elements;
}

export default class Root extends React.Component {
  static propTypes = {
    history: React.PropTypes.object.isRequired
  }

  render () {
    return (
      <Provider store={store} key="provider">
        <div>
            {createElements(this.props.history)}
        </div>
      </Provider>
    );
  }
}
