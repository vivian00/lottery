import React from 'react';

class Test extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Test';
  }

  render() {
    return (
      <div>
        I'm in here!!
      </div>
    );
  }
}

export default Test;
