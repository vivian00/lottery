/* eslint-disable  */
import React from 'react';
import { bindActionCreators } from 'redux';
import CommentItem from '../components/commentItem/CommentItem';
import { addPraise } from '../actions/index';
class CommentsContainer extends React.Component {

  render() {
    const { comments, order } = this.props.CommentsContainer;

    let commentsList = null;

    if( !comments || order.length > 0 ) {
      commentsList = order.map((id, index) => (
        <CommentItem
          key={index}
          data={comments[id]}
          onAddPraise={id => {
           this.props.addPraise(id);
            console.log(id);
            }
          }
        />)
      );
    }

    return (
      <ul>
        {commentsList}
      </ul>
    );
  }
}

CommentsContainer.propTypes = {
  CommentsContainer: React.PropTypes.shape({
    comments: React.PropTypes.object,
    order: React.PropTypes.array,
  }),
  addPraise: React.PropTypes.func
};

import { connect } from 'react-redux';

//  对从 redux 的 store 来的 state 做过滤
const mapStateToProps = state => {
  return {
    CommentsContainer: {
      comments: state.commentsById,
      order: state.commentsByOrder,
    }
  };
};

// 将dispatch 和相关的action 注入
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ addPraise }, dispatch);
}

// 用 connenct 包装 container
// 注入 dispatch 和全局 state
// action 调用 dispatch 的话， 要加 thunk
export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);

