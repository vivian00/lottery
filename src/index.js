/* global __DEVTOOLS__ */
import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
// hashHistory
import Root from './containers/Root';
// common js
import './commons/common';
// style
import './styles/core.scss';

ReactDOM.render(
  <Root history={browserHistory} />,
  document.getElementById('app'),
);
