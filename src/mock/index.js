 /**
 * Mocking client-server processing
 */
import _comments from './comments';

const TIMEOUT = 100;

export default {
  getComments: (fn, t) => setTimeout(() => fn(_comments), t || TIMEOUT),
};
