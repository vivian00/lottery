import tableDatas from './table';

module.exports = [
  {
    pattern: 'http://localhost.com/api/table',
    // pattern: 'http://wuguishifu.com/api/mentors/(\\d+)/comments/(.*)',
    // fixtures: function fixtures(match, params, headers) {
    fixtures: function fixtures() {
      const data = tableDatas;
      return data;
    },
    get: function get(match, data) {
      return {
        body: data,
      };
    },
    // post: function (match, data){
    post: function post() {
      return {
        code: 'k01',
      };
    },
  },
];
