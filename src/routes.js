/* eslint-disable */
import { Route, IndexRoute, browserHistory, hashHistory } from 'react-router';
import React from 'react';
import App from './containers/app/App';
import Test from './containers/test';
import Main from './containers/main/Main';
import Search from './containers/search/Search';
import CurrentSearch from './containers/currentSearch/CurrentSearch';

// const history = process.env.NODE_ENV !== 'production' ? browserHistory : hashHistory;
const routes = (
  <Route path="/" component={App}>
    <IndexRoute component={Main} />
    <Route path="test" component={Test} />
    <Route path="main" component={Main} />
    <Route path="search" component={Search} />
    <Route path="current-search" component={CurrentSearch} />
  </Route>
);
export default routes;
