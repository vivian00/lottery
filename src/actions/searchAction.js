/* eslint-disable */
import ajax from 'superagent';
export const REQUEST_POST = 'REQUEST_POST';
export const RECEIVE_SEARCH = 'RECEIVE_SEARCH';

// 获取数据成功
const receiveSearch = (data) => ({
  type: RECEIVE_SEARCH,
  data,
});

// 搜索获取数据
export const search = (url, params) => {
  return dispatch => {
    console.log('search!');
    console.log(params);
    ajax
      .get(url)
      .query(params)
      .then(res => {
        console.log('get Data!');
        console.log(res);
        dispatch(receiveSearch(res));
      });
  };
};
