import mock from '../mock/index';
// action 类型
export const ADD_PRAISE = 'ADD_PRAISE';
export const RECEIVE_STATE = 'RECEIVE_STATE';
export const GET_TABLES = 'GET_TABLES';

const receiveState = (comments = []) => ({
  type: RECEIVE_STATE,
  data: comments,
});

// 模拟数据
// 最后要 dispatch(mockData, action);
export const getAllcomments = () => (dispatch) => {
  console.log('getcomments');
  mock.getComments(data => {
    dispatch(receiveState(data));
  });
};

// action 创建函数
// 返回一个 action
export const addPraise = (id) => ({
  type: ADD_PRAISE,
  id,
});

export const getTables = (datas = []) => ({
  type: GET_TABLES,
  datas,
});
